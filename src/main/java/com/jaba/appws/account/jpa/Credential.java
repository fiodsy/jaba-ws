///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.jaba.appws.account.jpa;
//
//import com.jaba.appws.common.entity.BaseEntity;
//import com.jaba.appws.common.vo.Access;
//import javax.persistence.Column;
//import javax.persistence.Embedded;
//import javax.persistence.Entity;
//import javax.persistence.PrePersist;
//import javax.persistence.Table;
//import lombok.AccessLevel;
//import lombok.Data;
//import lombok.Getter;
//import lombok.Setter;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.repository.CrudRepository;
//import org.springframework.stereotype.Repository;
//
///**
// *
// * @author jaba
// */
//@Data
//@Entity
//@Table(name = "Credential")
//public class Credential extends BaseEntity {
//
//    private static final long serialVersionUID = -3889019879266742251L;
//
//    public Credential(String username, String password, Access access) {
//        this.username = username;
//        this.password = password;
//        this.access = access;
//    }
//
//    public Credential(String username, String password) {
//        this.username = username;
//        this.password = password;
//    }
//
//    @Column(nullable = true, unique = true)
//    private String username;
//
//    @Column(nullable = true)
//    private String password;
//
//    @Column(columnDefinition = "tinyint(1) default 0")
//    private boolean isVerified;
//
//    @Embedded
//    private Access access;
//
//    public Credential markAsVerified() {
//        this.isVerified = Boolean.TRUE;
//        return this;
//    }
//    
//    public Credential asSupplier(){
//        this.access.getType().setUser();
//        return this;
//    }
//
//    @PrePersist
//    private void onPrePersist() {
//        this.isVerified = Boolean.FALSE;
//    }
//}
