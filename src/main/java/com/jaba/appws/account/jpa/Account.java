/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jaba.appws.account.jpa;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonView;
import com.jaba.appws.account.domain.AccountRepository;
import com.jaba.appws.account.domain.IAccountService;
import com.jaba.appws.common.entity.AbstractBaseAggregate;
import com.jaba.appws.common.util.DataView;
import com.jaba.appws.common.vo.Access;
import com.jaba.appws.common.vo.AccessType;
import com.jaba.appws.common.vo.UserDetail;
import io.swagger.annotations.ApiModelProperty;
import java.util.logging.Logger;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Transient;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 *
 * @author jaba
 */
@Getter
@Setter
@Entity
@Table(name = "Account")
@DynamicUpdate(true)
public class Account extends AbstractBaseAggregate {

    private static final Logger LOG = Logger.getLogger(Account.class.getName());

    private static final long serialVersionUID = -6402127202621320535L;

//
//    @Autowired
//    transient private Credential.CredentialRepository credentialRepository;
    @ApiModelProperty(position = 0)
    @Enumerated(EnumType.STRING)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonView(DataView.Detail.class)
    private Type type;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "person", referencedColumnName = "id")
    @JsonView(DataView.Summary.class)
    private Person person;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "organization", referencedColumnName = "id")
    @JsonView(DataView.Summary.class)
    private Organization organization;

    @Column(unique = true, name = "username")
    @JsonView(DataView.Detail.class)
    private String username;

    @Column
    @JsonIgnore
    private String password;

    @Column(columnDefinition = "tinyint(1) default 0")
    @JsonView(DataView.Detail.class)
    private boolean isVerified;

    @Embedded
    @JsonView(DataView.Detail.class)
    private Access access;

    @JsonIgnore
    transient private UserDetail userDetail;

    public Account() {
        this.person = null;
        this.type = null;
    }

    public Account(Person person) {
        this.person = person;
        this.type = Type.PERSON;
    }

    public Account(Organization organization) {
        this.organization = organization;
        this.type = Type.ORGANIZATION;
    }

    public Account(String id, Person person) {
        this.person = person;
        this.type = Type.PERSON;
        this.setId(id);
    }

    public Account(String username, String password) {
        this.username = username;
        this.password = password;
        this.person = null;
        this.type = null;
    }

    public enum Type {
        PERSON, ORGANIZATION
    }

//    public Account from(Person person) {
//        this.type = Type.PERSON;
//        this.person = person;
//        return this;
//    }
//
//    public Account from(Organization organization) {
//        this.type = Type.ORGANIZATION;
//        this.organization = organization;
//        return this;
//    }
    public Account as(String role) {
        this.access = new Access(role);
        return this;
    }

    public Account markAsVerified() {
        this.isVerified = Boolean.TRUE;
        return this;
    }

    @PrePersist
    private void onPrePersist() {
        this.isVerified = Boolean.FALSE;
    }

    public UserDetail getUserDetail() {
        UserDetail userDetail = new UserDetail();
        if (this.type == Type.PERSON) {
            userDetail.setAccountType(Type.PERSON.toString());
            userDetail.setName(person.getName().getFirstName() + " " + person.getName().getLastName());
            Address address = person.getAddress();
            userDetail.setAddress(address.getAddressLine1() + " "
                    + address.getAddressLine2() + " "
                    + address.getCity() + ", "
                    + address.getProvince() + " "
                    + address.getZipCode() + " "
                    + address.getCountry());
            Contact contact = person.getContact();
            userDetail.setContactNumber("Mobile : " + contact.getMobileNumber() + " Telephone : " + contact.getPhoneNumber());
            userDetail.setEmail(contact.getEmail());
            userDetail.setId(getId());
        } else {
            userDetail.setAccountType(Type.ORGANIZATION.toString());
            Address address = organization.getAddress();
            userDetail.setName(organization.getName());
            userDetail.setAddress(address.getAddressLine1() + " "
                    + address.getAddressLine2() + " "
                    + address.getCity() + ", "
                    + address.getProvince() + " "
                    + address.getZipCode() + " "
                    + address.getCountry());
            Contact contact = organization.getContact();
            userDetail.setContactNumber("Mobile : " + contact.getMobileNumber() + "Telephone : " + contact.getPhoneNumber());
            userDetail.setEmail(contact.getEmail());
            userDetail.setId(getId());
        }
        return userDetail;
    }

    @Override
    public String toString() {
        return "Account{" + "type=" + type + ", person=" + person + ", username=" + username + ", password=" + password + ", isVerified=" + isVerified + ", access=" + access + '}';
    }

}
