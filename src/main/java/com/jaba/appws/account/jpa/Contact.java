/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jaba.appws.account.jpa;

import com.fasterxml.jackson.annotation.JsonView;
import com.jaba.appws.common.entity.BaseEntity;
import com.jaba.appws.common.util.DataView;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;

/**
 *
 * @author jaba
 */
@Data
@Entity
@Table(name = "Contact")
@DynamicUpdate(true)
public class Contact extends BaseEntity {

    private static final long serialVersionUID = 6314624916070130561L;

    @Pattern(regexp = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message = "Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 60)
    @Column(length = 60)
    @JsonView(DataView.Summary.class)
    private String email;

    @Size(min = 1, max = 20)
    @Column(length = 20)
    @JsonView(DataView.Summary.class)
    private String phoneNumber;

    @Column(length = 20)
    @JsonView(DataView.Summary.class)
    private String mobileNumber;

    @Column(length = 20)
    @JsonView(DataView.Summary.class)
    private String fax;

}
