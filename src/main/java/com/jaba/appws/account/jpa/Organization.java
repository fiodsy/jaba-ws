/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jaba.appws.account.jpa;

import com.fasterxml.jackson.annotation.JsonView;
import com.jaba.appws.common.entity.BaseEntity;
import com.jaba.appws.common.util.DataView;
import io.swagger.annotations.ApiModelProperty;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;

/**
 *
 * @author jaba
 */
@Data
@Entity
@Table(name = "Organization")
@DynamicUpdate
public class Organization extends BaseEntity {

    private static final long serialVersionUID = 5143113264875297174L;

    @ApiModelProperty(position = 1)
    @JsonView(DataView.Summary.class)
    private String name;

    @ApiModelProperty(position = 2)
    //@JsonUnwrapped
    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "address", referencedColumnName = "id")
    @JsonView(DataView.Summary.class)
    private Address address;

    @ApiModelProperty(position = 3)
    //@JsonUnwrapped
    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "contact", referencedColumnName = "id")
    @JsonView(DataView.Summary.class)
    private Contact contact;

}
