/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jaba.appws.account.jpa;

import com.fasterxml.jackson.annotation.JsonView;
import com.jaba.appws.common.entity.BaseEntity;
import com.jaba.appws.common.util.DataView;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;

/**
 *
 * @author jaba
 */
@Data
@Entity
@Table(name = "Address")
@DynamicUpdate(true)
public class Address extends BaseEntity {

    private static final long serialVersionUID = 4352703717987078899L;

    @Size(max = 50)
    @Column(length = 50)
    @JsonView(DataView.Summary.class)
    private String addressLine1;

    @Size(max = 50)
    @Column(length = 50)
    @JsonView(DataView.Summary.class)
    private String addressLine2;

    @Size(max = 45)
    @Column(length = 45)
    @JsonView(DataView.Summary.class)
    private String city;

    @Size(max = 45)
    @Column(length = 45)
    @JsonView(DataView.Summary.class)
    private String province;

    @Size(max = 45)
    @Column(length = 45)
    @JsonView(DataView.Summary.class)
    private String country;

    @Size(max = 5)
    @Column(length = 5)
    @JsonView(DataView.Summary.class)
    private String zipCode;

}
