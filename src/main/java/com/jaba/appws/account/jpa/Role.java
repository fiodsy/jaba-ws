/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jaba.appws.account.jpa;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.jaba.appws.common.entity.BaseLookUp;
import com.jaba.appws.common.vo.AccessType;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.persistence.AttributeOverride;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Data;

/**
 *
 * @author jaba
 */
@Data
@Entity
@Table(name = "Role")
public class Role extends BaseLookUp {

    private static final long serialVersionUID = -5622460522332501015L;

    private String permissions;

    @ApiModelProperty(position = 0)
    @Enumerated(EnumType.STRING)
    Type type;

    public enum Type {
        ADMIN, USER
    }

    public Role setUserType() {
        this.type = Role.Type.USER;
        return this;
    }

    public Role setAdminType() {
        this.type = Role.Type.ADMIN;
        return this;
    }

}
