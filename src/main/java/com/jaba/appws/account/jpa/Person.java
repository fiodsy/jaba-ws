/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jaba.appws.account.jpa;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.fasterxml.jackson.annotation.JsonView;
import com.jaba.appws.common.entity.AbstractBaseAggregate;
import com.jaba.appws.common.entity.BaseEntity;
import com.jaba.appws.common.util.DataView;
import com.jaba.appws.common.vo.Name;
import io.swagger.annotations.ApiModelProperty;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;

/**
 *
 * @author jaba
 */
@Data
@Entity
@Table(name = "Person")
@DynamicUpdate(true)
public class Person extends BaseEntity {

    private static final long serialVersionUID = 199710636317794302L;

    @ApiModelProperty(position = 1)
    @JsonUnwrapped
    @Embedded
    @JsonView(DataView.Summary.class)
    private Name name;

    @ApiModelProperty(position = 3)
    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "contact", referencedColumnName = "id")
    @JsonView(DataView.Summary.class)
    private Contact contact;

    @ApiModelProperty(position = 2)
    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "address", referencedColumnName = "id")
    @JsonView(DataView.Summary.class)
    private Address address;

}
