/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jaba.appws.account.domain.controller;

import com.jaba.appws.account.jpa.Account;
import com.jaba.appws.common.dto.UsernamePasswordDto;
import com.jaba.appws.common.security.JwtTokenProvider;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author jaba
 */
@Controller
@Api(tags = "Authorization")
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    private AuthenticationManager authenticationManager;
    
    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @PostMapping
    @ApiOperation(value = "Login Account")
    public ResponseEntity<?> authenticateAccount(@Valid @RequestBody UsernamePasswordDto upd) {
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(upd.getUsername(), upd.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        System.out.println(" APPLICATION " + authentication.getName());
        String jwt = jwtTokenProvider.generateToken(authentication);
        return ResponseEntity.ok(jwt);
//eturn ResponseEntity.created(linkTo(AccountController.class).slash("asd").toUri()).build();
    }

}
