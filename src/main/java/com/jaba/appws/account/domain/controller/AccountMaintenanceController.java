/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jaba.appws.account.domain.controller;

import com.jaba.appws.account.domain.IAccountMaintenanceService;
import com.jaba.appws.account.jpa.Permission;
import com.jaba.appws.account.jpa.Role;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author jaba
 */
@Controller
@Api(tags = "Account Maintenance")
@RequestMapping("/maintenance/account")
public class AccountMaintenanceController {

    @Autowired
    private IAccountMaintenanceService maintenanceService;

    @PostMapping(path = "/role")
    @ApiOperation(value = "Create Role")
    public ResponseEntity<?> createRole(@RequestBody Role role, @RequestParam Type type) {
        Role roleResponse = maintenanceService.createRole(role).setUserType();
        return ResponseEntity.created(linkTo(AccountMaintenanceController.class).slash("/maintenance").slash("/role").slash(roleResponse.getCode()).toUri()).build();
    }

    @PostMapping(path = "/permission")
    @ApiOperation(value = "Create Permission")
    public ResponseEntity<?> createPermission(@RequestBody Permission permission) {
        Permission permissionResponse = maintenanceService.createPermission(permission);
        return ResponseEntity.created(linkTo(AccountMaintenanceController.class).slash("/maintenance").slash("/permission").slash(permissionResponse.getCode()).toUri()).build();
    }
    
     @PutMapping(path = "/role")
    @ApiOperation(value = "Update Role")
    public ResponseEntity<?> updateRole(@RequestBody List<String> permissions, @RequestParam RoleEnum role) {
        Role roleResponse = maintenanceService.setPermissionsToRole(role.name(), permissions);
        return ResponseEntity.created(linkTo(AccountMaintenanceController.class).slash("/maintenance").slash("/role").slash(roleResponse.getCode()).toUri()).build();
    }

    public enum Type {
        ADMIN, USER
    }
    
      public enum RoleEnum {
        SUPAD, SUPP, CUSTM
    }

}
