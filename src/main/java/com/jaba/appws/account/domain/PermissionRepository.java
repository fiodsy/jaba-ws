/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jaba.appws.account.domain;

import com.jaba.appws.account.jpa.Permission;
import com.jaba.appws.account.jpa.Role;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jaba
 */
@Repository
public interface PermissionRepository extends CrudRepository<Permission, String>{
    Permission findByCode(String code);
    
}