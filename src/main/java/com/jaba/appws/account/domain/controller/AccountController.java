/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jaba.appws.account.domain.controller;

import com.fasterxml.jackson.annotation.JsonView;
import com.google.gson.Gson;
import com.jaba.appws.account.domain.AccountRepository;
import com.jaba.appws.account.domain.IAccountService;
import com.jaba.appws.account.domain.RoleRepository;
import com.jaba.appws.account.jpa.Account;
import com.jaba.appws.account.jpa.Organization;
import com.jaba.appws.account.jpa.Person;
import com.jaba.appws.common.dto.UsernamePasswordDto;
import com.jaba.appws.common.security.UserPrincipal;
import com.jaba.appws.common.util.DataView;
import com.jaba.appws.common.vo.Access;
import com.jaba.appws.error.JabaException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.security.Principal;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author jaba
 */
@RestController
@Api(tags = "Account Controller")
@RequestMapping("/account")
public class AccountController {

    @Autowired
    private IAccountService accountService;

    @Autowired
    private RoleRepository roleRepository;

    @PostMapping
    @ApiOperation(value = "Create Account")
    public ResponseEntity<?> createAccount(@RequestBody UsernamePasswordDto upd, @RequestParam Role role) {
        Account account = accountService.registerAccount(upd.getUsername(), upd.getPassword(), role.getCode());
        return ResponseEntity.created(linkTo(AccountController.class).slash(account.getId()).toUri()).build();
    }

    @PutMapping(value = "/person/{id}")
    @ApiOperation(value = "Update Person Account")
    public ResponseEntity<?> updatePersonAccount(@PathVariable("id") String id, Principal principal, @RequestBody Person person, @RequestParam Type type) {

        Account account = accountService.updatePersonAccount(id, person);
        System.out.println("USERNAME - - - " + principal.getName());
        return ResponseEntity.created(linkTo(AccountController.class).slash(account.getId()).toUri()).build();
    }

    @PutMapping(value = "/organization/{id}")
    @ApiOperation(value = "Update Organization Account")
    public ResponseEntity<?> updateOrganizationAccount(@PathVariable("id") String id, @RequestBody Organization organization, @RequestParam Type type) {
        Account account = accountService.updateOrganizationAccount(id, organization);
        return ResponseEntity.created(linkTo(AccountController.class).slash(account.getId()).toUri()).build();
    }

    @GetMapping(value = "/{id}")
    @ApiOperation(value = "Get Account")
    @JsonView(DataView.Summary.class)
    public ResponseEntity<?> getAccount(@PathVariable("id") String id) throws JabaException {
        Account account = accountService.getAccountById(id);
        return ResponseEntity.ok(account);
    }

//    @GetMapping(value = "/{id}/persons")
//    @ApiOperation(value = "Get Person Detail")
//    @JsonView(DataView.Summary.class)
//    public ResponseEntity<?> getPersonDetail(@PathVariable("id") String id) throws JabaException {
//        Account account = accountService.getAccountById(id);
//        return ResponseEntity.ok(account);
//    }
//
//    @GetMapping(value = "/{id}/organizations")
//    @ApiOperation(value = "Get organization Detail")
//    @JsonView(DataView.Summary.class)
//    public ResponseEntity<?> getOrganizationDetail(@PathVariable("id") String id) throws JabaException {
//        Account account = accountService.getAccountById(id);
//        return ResponseEntity.ok(account);
//    }

    public enum Role {
        SUPPLIER("SUPP"), CUSTOMER("CUSTM");

        private String code;

        public String getCode() {
            return code;
        }

        Role(String code) {
            this.code = code;
        }
    }

    public enum Type {
        Person("PER"), Organization("ORG");

        private String code;

        public String getCode() {
            return code;
        }

        Type(String code) {
            this.code = code;
        }
    }

}
