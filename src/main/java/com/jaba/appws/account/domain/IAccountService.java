/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jaba.appws.account.domain;

import com.jaba.appws.account.jpa.Account;
import com.jaba.appws.account.jpa.Organization;
import com.jaba.appws.account.jpa.Person;
import com.jaba.appws.common.vo.Access;
import com.jaba.appws.error.JabaException;

/**
 *
 * @author jaba
 */
public interface IAccountService {

    Account registerAccount(String username, String password, String role);

    Account getAccountById(String id) throws JabaException;

    Account findByUsernameOrEmail(String usernameOrEmail);

    Account updatePersonAccount(String id, Person person);

    Account updateOrganizationAccount(String id, Organization organization);
    
    
}
