/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jaba.appws.account.domain;

import com.jaba.appws.account.jpa.Account;
import com.jaba.appws.account.jpa.Organization;
import com.jaba.appws.account.jpa.Person;
import com.jaba.appws.common.util.MapUtil;
import com.jaba.appws.common.vo.Access;
import com.jaba.appws.error.ErrorCode;
import com.jaba.appws.error.JabaException;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 *
 * @author jaba
 */
@Service
public class AccountServiceImpl implements IAccountService {

    private static final Logger LOG = Logger.getLogger(AccountServiceImpl.class.getName());

    @Autowired
    EntityManager em;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public Account registerAccount(String username, String password, String role) {
        Account account = accountRepository.save(new Account(username, passwordEncoder.encode(password)).as(role));
        LOG.info("ACCOUNT --- " + account.toString() + " ROLE " + role);
        return account;
    }

    @Override
    public Account getAccountById(String id) throws JabaException {
        Account account = null;
        account = accountRepository.findById(id);
        if (account == null) {
            throw new JabaException(ErrorCode.Sample.SAMPLE_CODE, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        String role = account.getAccess().getRole();
        account.getAccess().setPermissions(StringUtils.commaDelimitedListToSet(roleRepository.findByCode(role).getPermissions()));
        return account;
    }

    @Override
    public Account findByUsernameOrEmail(String usernameOrEmail) {
        Account account = accountRepository.findByUsername(usernameOrEmail);
        String role = account.getAccess().getRole();
        account.getAccess().setPermissions(StringUtils.commaDelimitedListToSet(roleRepository.findByCode(role).getPermissions()));
        return account;
    }

    @Override
    public Account updatePersonAccount(String id, Person person) {
        Account account = accountRepository.findById(id);
        if (person.getAddress() == null) {
            person.setAddress(account.getPerson().getAddress());
        }
        if (person.getContact() == null) {
            person.setContact(account.getPerson().getContact());
        }
        if (person.getName() == null) {
            person.setName(account.getPerson().getName());
        }
        accountRepository.save(MapUtil.MAPPER.updateAccount(new Account(person), account));
        return account;
    }

    @Override
    public Account updateOrganizationAccount(String id, Organization organization) {
        Account account = accountRepository.findById(id);
        if (organization.getAddress() == null) {
            organization.setAddress(account.getOrganization().getAddress());
        }
        if (organization.getContact() == null) {
            organization.setContact(account.getOrganization().getContact());
        }
        accountRepository.save(MapUtil.MAPPER.updateAccount(new Account(organization), account));
        return account;
    }

}
