/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jaba.appws.account.domain;

import com.jaba.appws.account.jpa.Permission;
import com.jaba.appws.account.jpa.Role;
import com.jaba.appws.common.dto.LookUpDto;
import java.util.List;

/**
 *
 * @author jaba
 */
public interface IAccountMaintenanceService {
    Role createRole(Role role);

    Permission createPermission(Permission permission);
    
    Role setPermissionsToRole(String roleCode, List<String> permissions);
}
