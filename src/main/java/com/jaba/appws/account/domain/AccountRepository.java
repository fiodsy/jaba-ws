/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jaba.appws.account.domain;

import com.jaba.appws.account.jpa.Account;
import java.io.Serializable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jaba
 */
@Repository
public interface AccountRepository extends CrudRepository<Account, String> {

    Account findByUsername(String username);

    Account findById(String id);

}
