/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jaba.appws.account.domain;

import com.jaba.appws.account.jpa.Permission;
import com.jaba.appws.account.jpa.Role;
import com.jaba.appws.common.dto.LookUpDto;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 *
 * @author jaba
 */
@Service
public class AccountMaintenanceServiceImpl implements IAccountMaintenanceService {

    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private PermissionRepository permissionRepository;

    @Override
    public Role createRole(Role role) {
        return roleRepository.save(role);
    }

    @Override
    public Permission createPermission(Permission permission) {
        return permissionRepository.save(permission);
    }

    @Override
    public Role setPermissionsToRole(String roleCode, List<String> permissions) {
        Role role = roleRepository.findByCode(roleCode);
        Set<String> existingPermissions = StringUtils.commaDelimitedListToSet(role.getPermissions());
        permissions.forEach(permission -> existingPermissions.add(permissionRepository.findByCode(permission).getCode()));
        role.setPermissions(StringUtils.collectionToCommaDelimitedString(existingPermissions));
        return roleRepository.save(role);
    }

}
