/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jaba.appws.error;

import java.util.ResourceBundle;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author jaba
 */
@Getter
@Setter
@ResponseStatus(HttpStatus.NOT_FOUND)
public class JabaException extends Exception {

    private static final long serialVersionUID = -6730903445744560605L;

    private String errorCode;

    private String field;

    private String rejectedValue;

    private HttpStatus status;

    private static ResourceBundle properties = ResourceBundle.getBundle("errormessages");

    public JabaException(String errorCode, String field, String rejectedValue, HttpStatus status, String message) {
        super(message);
        this.errorCode = errorCode;
        this.field = field;
        this.rejectedValue = rejectedValue;
        this.status = status;
    }

    public JabaException(String errorCode, HttpStatus status) {
        this.errorCode = errorCode;
        this.status = status;
    }

    public JabaException(String errorCode, HttpStatus status, Throwable cause) {
        super(properties.getString(errorCode), cause);
        this.errorCode = errorCode;
        this.status = status;
    }

    public JabaException(String errorCode) {
        super(errorCode);
    }

}
