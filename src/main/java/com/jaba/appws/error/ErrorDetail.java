/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jaba.appws.error;

import lombok.Data;
import lombok.Getter;

/**
 *
 * @author jaba
 */
@Data
public class ErrorDetail {
    
    private String field;
    private String rejectedValue;
    private String message;
    
}
