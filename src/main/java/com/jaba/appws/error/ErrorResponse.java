/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jaba.appws.error;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.List;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.http.HttpStatus;

/**
 *
 * @author jaba
 */
@Builder(builderMethodName = "timeStampBuilder")
@ToString
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ErrorResponse {
    
    private final String status;
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private final ZonedDateTime timeStamp;
    private final String errorCode;
    private final String message;
    private final String path;
    private final List<ErrorDetail> details;

    public static ErrorResponseBuilder builder() {
        return timeStampBuilder().timeStamp(ZonedDateTime.now());
    }

}
