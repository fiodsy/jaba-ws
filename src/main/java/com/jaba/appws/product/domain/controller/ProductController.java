/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jaba.appws.product.domain.controller;

import com.fasterxml.jackson.annotation.JsonView;
import com.google.gson.Gson;
import com.jaba.appws.account.domain.IAccountService;
import com.jaba.appws.account.jpa.Account;
import com.jaba.appws.common.util.DataView;
import com.jaba.appws.common.util.MapUtil;
import com.jaba.appws.error.JabaException;
import com.jaba.appws.product.domain.IProductService;
import com.jaba.appws.product.domain.specification.ItemSearchParameter;
import com.jaba.appws.product.jpa.Category;
import com.jaba.appws.product.jpa.Item;
import com.jaba.appws.product.jpa.Product;
import com.jaba.appws.product.jpa.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.security.Principal;
import org.springframework.beans.factory.annotation.Autowired;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author jaba
 */
@Controller
@Api(tags = "Product")
@RequestMapping("/product")
public class ProductController {

    @Autowired
    private IProductService productService;

    @Autowired
    private IAccountService accountService;

    @PostMapping(path = "/items")
    @ApiOperation(value = "Create Item")
    public ResponseEntity<?> createItem(@RequestBody Item item, Principal principal) throws JabaException {
        Product product = new Product(item).of(getCurrentUser(principal));
        product = productService.createItem(product);
        return ResponseEntity.created(linkTo(ProductController.class).slash("/items").slash(product.getItem().getId()).toUri()).build();
    }

    @PutMapping(path = "/items/{id}")
    @ApiOperation(value = "Update Item")
    public ResponseEntity<?> updateItem(@PathVariable("id") String id, @RequestBody Item item, Principal principal) throws JabaException {
        item.setId(id);
        Product product = new Product(item).of(getCurrentUser(principal));
        System.out.println("PRODUCT " + product.toString());
        product = productService.updateSupplierItem(product);
        return ResponseEntity.created(linkTo(ProductController.class).slash("/item").slash(product.getItem().getId()).toUri()).build();
    }

//    @GetMapping(path = "/catalogs")
//    @ApiOperation(value = "Get Catalogs")
//    public ResponseEntity<?> fetchItems(Principal principal) throws JabaException {
//        Product product = productService.fetchCatalogs();
//        return ResponseEntity.ok(product.toItemCatalogResource());
//    }
    @GetMapping(path = "/catalogs")
    @ApiOperation(value = "Get Catalogs")
    public ResponseEntity<?> fetchCatalogs(Principal principal) throws JabaException {
        Product product = productService.fetchAllItems();
        return ResponseEntity.ok(product.toListItemCatalogResource());
    }

    @GetMapping(path = "/items/{id}")
    @ApiOperation(value = "Get Item")
    public ResponseEntity<?> getItem(@PathVariable("id") String id, Principal principal) {
        Item item = new Item();
        item.setId(id);
        Product product = new Product(item);
        product = productService.getItem(product);
        return ResponseEntity.ok(product.toItemResource());
    }

    @GetMapping(path = "/items")
    @ApiOperation(value = "Fetch Item")
    public ResponseEntity<?> getItem(ItemSearchParameter itemSearchParameter, Principal principal) throws JabaException {

        Product product = Product.instance();
        product.setItemSearchParameter(itemSearchParameter);
        product = productService.fetchAllItemsBySpecification(product);
        return ResponseEntity.ok(product.toListItemCatalogResource());
    }

    @GetMapping(path = "/supplier/{supplier}/items")
    @ApiOperation(value = "Get Items by Supplier")
    public ResponseEntity<?> getItemBySupplier(@PathVariable("supplier") String supplier, Principal principal) throws JabaException {
        Product product = Product.fetchBy().supplierId(supplier);
        System.out.println("PRODUCT " + product.toString());
        return ResponseEntity.ok(productService.fetchSupplierItems(product));
    }

    @GetMapping(path = "/supplier/{supplier}/catalogs")
    @ApiOperation(value = "Get Catalogs by Supplier")
    public ResponseEntity<?> getCatalogsBySupplier(@PathVariable("supplier") String supplier, Principal principal) throws JabaException {
        Product product = Product.fetchBy().supplierId(supplier);
        product = productService.fetchSupplierItems(product);
        System.out.println("PRODUCT " + product.toString());
        return ResponseEntity.ok(product.toListItemCatalogResource());
    }

    @GetMapping(path = "/supplier/{supplier}/items/{item}")
    @ApiOperation(value = "Get Items by Supplier")
    public ResponseEntity<?> getItemsBySupplier(@PathVariable("supplier") String supplier, @PathVariable("item") String item, Principal principal) throws JabaException {
        Product product = Product.fetchBy().supplierId(supplier).itemId(item);
        System.out.println("PRODUCT " + product.toString());
        return ResponseEntity.ok(productService.getSupplierItemById(product));
    }

    protected User getCurrentUser(Principal principal) {
        Account account = accountService.findByUsernameOrEmail(principal.getName());
        return MapUtil.MAPPER.mapUserDetailToUser(account.getUserDetail());
    }

}
