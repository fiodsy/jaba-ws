/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jaba.appws.product.domain.specification;

import com.jaba.appws.product.jpa.Type;
import com.jaba.appws.product.jpa.Type.Category;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 *
 * @author jaba
 */
@Data
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class ItemSearchParameter {

    private String id;

    private String itemName;

    private String supplierName;

    private String type;

    private Category category;

    private String supplierId;

}
