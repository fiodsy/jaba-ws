/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jaba.appws.product.domain.specification;

import com.google.common.base.Predicate;
import com.jaba.appws.common.specification.AbstractSpecificationTemplate;
import com.jaba.appws.product.jpa.Item;
import com.jaba.appws.product.jpa.User;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.ListJoin;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.jpa.domain.Specification;
import static org.springframework.data.jpa.domain.Specifications.where;
import org.springframework.stereotype.Component;

/**
 *
 * @author jaba
 */
@Component
public class ItemSpecificationBuilder extends AbstractSpecificationTemplate<Item, ItemSearchParameter> {

    public Specification<Item> getFilter(ItemSearchParameter itemSearchParameter) {
        return (root, query, builder) -> {
            query.distinct(true);
            Specification<Item> specs = null;
            try {
                specs = withItemCategory(itemSearchParameter.getCategory().getCode());
            } catch (NullPointerException e) {

            }

            return where(where(equal("id", itemSearchParameter.getId()))
                    .and(like("name", itemSearchParameter.getItemName()))
                    .and(supplierEqual("referenceId", itemSearchParameter.getSupplierId()))
                    .and(supplierContains("name", itemSearchParameter.getSupplierName())))
                    .and(specs)
                    .toPredicate(root, query, builder);
        };
    }

    private Specification<Item> withItemCategory(String value) {
        String key = "type";
        if (!isValueValid(value)) {
            return null;
        }
        return (root, query, builder) -> {

            return builder.like(root.get(key), value + "%"); //To change body of generated lambdas, choose Tools | Templates.
        };
    }

    private Specification<Item> supplierContains(String key, String value) {
        return (root, query, builder) -> {
            if (!areInputsValid(key, value)) {
                return null;
            }
            Join<Item, User> users = root.join("supplier", JoinType.INNER);
            return builder.like(
                    builder.lower(users.get(key)),
                    containsLowerCase(value)
            );
        };
    }

    private Specification<Item> supplierEqual(String key, String value) {
        return (root, query, builder) -> {
            if (!areInputsValid(key, value)) {
                return null;
            }
            Join<Item, User> user = root.join("supplier", JoinType.INNER);
            return builder.equal(user.get(key), value);

        };
    }

}
