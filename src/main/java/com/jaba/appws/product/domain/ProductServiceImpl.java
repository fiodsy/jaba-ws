/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jaba.appws.product.domain;

import com.google.gson.Gson;
import com.jaba.appws.common.util.MapUtil;
import com.jaba.appws.error.JabaException;
import com.jaba.appws.product.domain.specification.ItemSpecificationBuilder;
import com.jaba.appws.product.jpa.Item;
import com.jaba.appws.product.jpa.Catalog;
import com.jaba.appws.product.jpa.Product;
import com.jaba.appws.product.jpa.User;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 *
 * @author jaba
 */
@Service
public class ProductServiceImpl implements IProductService {

    @Autowired
    private ItemRepository itemRepository;

    @Autowired
    private ItemSpecificationBuilder itemSpecificationBuilder;

    @Override
    public Product createItem(Product product) {
        User user = product.getUser();
        user.setRole(User.Role.SUPPLIER);
        product.setUser(user);
        Item item = product.getItem();
        item.setSupplier(user);
        return new Product(itemRepository.save(item));
    }

    @Override
    public Product updateSupplierItem(Product product) throws JabaException {
        product.setSupplier(product.getUser().getReferenceId());
        product.setItemId(product.getItem().getId());
        Product existingProduct = getSupplierItemById(product);
        Set<String> existingTags = StringUtils.commaDelimitedListToSet(existingProduct.getItem().getTags());
        existingTags.addAll(StringUtils.commaDelimitedListToSet(product.getItem().getTags()));
        product.getItem().setTags(StringUtils.collectionToCommaDelimitedString(existingTags));
        System.out.println("EXISTING PRODUCT " + product.toString());
        if (existingProduct != null) {
            Product updatedProduct = MapUtil.MAPPER.updateProduct(product, existingProduct);
            System.out.println("PRODUCT UPDATE " + product.toString());
            existingProduct.setItem(itemRepository.save(updatedProduct.getItem()));
        }

        return existingProduct;
    }

    @Override
    public Product getItem(Product product) {
        Item item = itemRepository.findById(product.getItem().getId());
        System.out.println("ITEM " + new Gson().toJson(item) + " ID " + product.getItem().getId());
        return new Product(item);
    }

    @Override
    public Product getSupplierItemById(Product product) throws JabaException {
        Item item = itemRepository.findBySupplierReferenceIdAndItemId(product.getSupplier(), product.getItemId());

        return new Product(item);
    }

    @Override
    public Product fetchAllItems() throws JabaException {
        List<Item> items = (List) itemRepository.findAll();
        Product product = Product.instance();
        product.setItems(items);
        return product;
    }

    @Override
    public Product fetchAllItemsBySpecification(Product product) throws JabaException {
        List<Item> items = (List) itemRepository.findAll(itemSpecificationBuilder.getFilter(product.getItemSearchParameter()));
        product.setItems(items);
        return product;
    }

    @Override
    public Product fetchSupplierItems(Product product) throws JabaException {
        List<Item> items = itemRepository.findBySupplierReferenceId(product.getSupplier());
        product.setItems(items);
        return product;
    }

    @Override
    public Product fetchCatalogs() throws JabaException {
        List<Catalog> catalogs = itemRepository.fetchCatalogs();
        return new Product(catalogs);
    }

}
