/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jaba.appws.product.domain;

import com.jaba.appws.product.jpa.Item;
import com.jaba.appws.product.jpa.Catalog;
import com.jaba.appws.product.jpa.Product;
import java.io.Serializable;
import java.util.List;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.SqlResultSetMapping;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jaba
 */
@Repository
public interface ItemRepository extends CrudRepository<Item, String>, JpaSpecificationExecutor<Item> {

    Item findById(String id);

    @Query(value = "SELECT i.* FROM product_item i INNER JOIN product_user u ON i.supplier = u.id WHERE u.reference_id = :referenceId",
            nativeQuery = true)
    List<Item> findBySupplierReferenceId(@Param("referenceId") String referenceId);

    @Query(value = "SELECT i.* FROM product_item i INNER JOIN product_user u ON i.supplier = u.id WHERE u.reference_id = :referenceId AND i.id = :itemId",
            nativeQuery = true)
    Item findBySupplierReferenceIdAndItemId(@Param("referenceId") String referenceId, @Param("itemId") String itemId);

    @Query(nativeQuery = true, name = "fetchCatalogs")
    List<Catalog> fetchCatalogs();
    
    
   

}
