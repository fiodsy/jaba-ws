/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jaba.appws.product.domain;

import com.jaba.appws.product.jpa.Category;
import com.jaba.appws.product.jpa.Type;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jaba
 */
@Service
public class ProductMaintenanceServiceImpl implements IProductMaintenanceService {

    @Autowired
    private ProductCategoryRepository categoryRepository;

    @Autowired
    private ProductTypeRepository typeRepository;

    @Override
    public Category createProductCategory(Category category) {
        return categoryRepository.save(category);
    }

    @Override
    public Type createProductType(Type type) {
        type.setCode(type.getCategory().getCode() + type.getCode());
        return typeRepository.save(type);
    }

}
