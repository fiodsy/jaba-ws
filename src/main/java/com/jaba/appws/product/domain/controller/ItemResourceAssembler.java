/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jaba.appws.product.domain.controller;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.jaba.appws.product.domain.controller.ItemResourceAssembler.ItemResource;
import com.jaba.appws.product.jpa.Item;
import lombok.Data;
import org.springframework.hateoas.ResourceSupport;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Component;

/**
 *
 * @author jaba
 */
@Component
public class ItemResourceAssembler extends ResourceAssemblerSupport<Item, ItemResource> {

    public ItemResourceAssembler() {
        super(Item.class, ItemResource.class);
    }

    @Override
    public ItemResource toResource(Item item) {
        ItemResource resource = new ItemResource(item);
        resource.add(linkTo(ProductController.class).slash("/items").slash(item.getId()).withSelfRel());
        return resource;
    }

    @Data
    public class ItemResource extends ResourceSupport {

        @JsonUnwrapped
        private final Item item;

    }

}
