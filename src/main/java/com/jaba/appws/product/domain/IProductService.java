/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jaba.appws.product.domain;

import com.jaba.appws.error.JabaException;
import com.jaba.appws.product.jpa.Item;
import com.jaba.appws.product.jpa.Product;

/**
 *
 * @author jaba
 */
public interface IProductService {

    Product createItem(Product product) throws JabaException;

    Product updateSupplierItem(Product product) throws JabaException;

    Product getItem(Product product) throws NullPointerException;

    Product getSupplierItemById(Product product) throws JabaException;

    Product fetchAllItems() throws JabaException;

    Product fetchSupplierItems(Product product) throws JabaException;

    Product fetchCatalogs() throws JabaException;

    Product fetchAllItemsBySpecification(Product product) throws JabaException;

}
