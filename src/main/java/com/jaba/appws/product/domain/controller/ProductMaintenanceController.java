/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jaba.appws.product.domain.controller;

import com.jaba.appws.account.domain.controller.*;
import com.jaba.appws.account.domain.IAccountMaintenanceService;
import com.jaba.appws.account.jpa.Permission;
import com.jaba.appws.account.jpa.Role;
import com.jaba.appws.product.domain.IProductMaintenanceService;
import com.jaba.appws.product.jpa.Category;
import com.jaba.appws.product.jpa.Type;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author jaba
 */
@Controller
@Api(tags = "Product Maintenance")
@RequestMapping("/maintenance/product")
public class ProductMaintenanceController {

    @Autowired
    private IProductMaintenanceService maintenanceService;

    @PostMapping(path = "/category")
    @ApiOperation(value = "Create Category")
    public ResponseEntity<?> createCategory(@RequestBody Category category) {
        Category categoryResponse = maintenanceService.createProductCategory(category);
        return ResponseEntity.created(linkTo(ProductMaintenanceController.class).slash("/maintenance").slash("/category").slash(categoryResponse.getCode()).toUri()).build();
    }
//
    @PostMapping(path = "/type")
    @ApiOperation(value = "Create Type")
    public ResponseEntity<?> createPermission(@RequestBody Type type, @RequestParam Type.Category category) {
        type.setCategory(category);
        Type typeResponse = maintenanceService.createProductType(type);
        return ResponseEntity.created(linkTo(ProductMaintenanceController.class).slash("/maintenance").slash("/type").slash(typeResponse.getCode()).toUri()).build();
    }
//    
//     @PutMapping(path = "/role")
//    @ApiOperation(value = "Update Role")
//    public ResponseEntity<?> updateRole(@RequestBody List<String> permissions, @RequestParam RoleEnum role) {
//        Role roleResponse = maintenanceService.setPermissionsToRole(role.name(), permissions);
//        return ResponseEntity.created(linkTo(ProductMaintenanceController.class).slash("/maintenance").slash("/role").slash(roleResponse.getCode()).toUri()).build();
//    }
//
//    public enum Type {
//        ADMIN, USER
//    }
//    
//      public enum RoleEnum {
//        SUPAD, SUPP, CUSTM
//    }

}
