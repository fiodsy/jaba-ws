/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jaba.appws.product.domain;

import com.jaba.appws.product.jpa.Category;
import com.jaba.appws.product.jpa.Type;

/**
 *
 * @author jaba
 */
public interface IProductMaintenanceService {

    Category createProductCategory(Category category);

    Type createProductType(Type type);
}
