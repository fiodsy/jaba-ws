/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jaba.appws.product.jpa;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonView;
import com.jaba.appws.common.entity.AbstractBaseId;
import com.jaba.appws.common.util.DataView;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Where;

/**
 *
 * @author jaba
 */
@Data
@Entity
@Table(name = "ProductItem")
@DynamicUpdate(true)
@Where(clause = "active=1")
@SqlResultSetMapping(
        name = "catalogResult", classes = {
            @ConstructorResult(targetClass = Catalog.class,
                    columns = {
                        @ColumnResult(name = "id", type = String.class)
                        ,
                @ColumnResult(name = "name", type = String.class)
                        ,
                @ColumnResult(name = "type", type = String.class)
                        ,
                @ColumnResult(name = "supplier", type = String.class)
                        ,
                @ColumnResult(name = "price", type = String.class)
                        ,
                @ColumnResult(name = "negotiable", type = Boolean.class)

                    })

        })
@NamedNativeQueries({
    @NamedNativeQuery(name = "fetchCatalogs",
            query = "SELECT i.id ,i.name, i.price, i.negotiable, t.name as type, u.name as supplier FROM product_item i inner join product_type t ON i.type = t.code inner join product_user u ON i.supplier = u.id",
            resultSetMapping = "catalogResult")
})
@JsonInclude(Include.NON_NULL)
public class Item extends AbstractBaseId {

    private static final long serialVersionUID = -7339076014432340912L;

    @Column
    @JsonView(DataView.Summary.class)
    private String name;

    @Lob
    @Column
    private String description;

    @Column
    @JsonView(DataView.Summary.class)
    private String type;

    @Column(columnDefinition = "tinyint(1) default 1")
    private boolean active;

    @Column
    @JsonView(DataView.Summary.class)
    private String currency;

    @Column
    @JsonView(DataView.Summary.class)
    private String price;

    @Column(columnDefinition = "tinyint(1) default 0")
    @JsonView(DataView.Summary.class)
    private boolean negotiable;

    @Lob
    @Column
    private String tags;

    @OneToMany(
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @JoinColumn(name = "image_id")
    @JsonIgnore
    private List<Image> image = new ArrayList<>();

    @ApiModelProperty(hidden = true)
    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "supplier", referencedColumnName = "id")
    @JsonView(DataView.Summary.class)
    private User supplier;

}
