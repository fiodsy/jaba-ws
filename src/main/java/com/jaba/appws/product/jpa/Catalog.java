/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jaba.appws.product.jpa;

import lombok.Data;

/**
 *
 * @author jaba
 */
@Data
public class Catalog {

    private String id;

    private String image;

    private String name;

    private String type;

    private String supplier;

    private String amount;

    private boolean negotiable;

    public Catalog(String id, String name, String type, String supplier, String amount, boolean negotiable) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.supplier = supplier;
        this.amount = amount;
        this.negotiable = negotiable;
    }
    
    

}
