/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jaba.appws.product.jpa;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.jaba.appws.account.domain.controller.AccountController;
import com.jaba.appws.account.jpa.Account;
import com.jaba.appws.common.util.DataView;
import com.jaba.appws.product.domain.controller.ProductController;
import com.jaba.appws.product.domain.specification.ItemSearchParameter;
import com.jaba.appws.product.domain.specification.ItemSpecificationBuilder;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import lombok.Data;
import org.springframework.hateoas.Resource;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

/**
 *
 * @author jaba
 */
@Data
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonInclude(Include.NON_NULL)
public class Product {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonView(DataView.Summary.class)
    private Item item;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private User user;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonView(DataView.Summary.class)
    private List<Item> items;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String supplier;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String itemId;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<Catalog> catalogs;

    transient ItemSearchParameter itemSearchParameter;

    public Product(Item item) {
        this.item = item;
    }

    private Product() {

    }

    public Product(List<Catalog> catalogs) {
        this.catalogs = catalogs;
    }

    public Product of(User user) {
        this.user = user;
        return this;
    }

    public static Product instance() {
        return new Product();
    }

    public static Product fetchBy() {
        return new Product();
    }

    public Product supplierId(String supplier) {
        this.supplier = supplier;
        return this;
    }

    public Product itemId(String itemId) {
        this.itemId = itemId;
        return this;
    }

    public class ProductResourceAssembler {

        public Resource<Item> toResource(Item item) {
            Resource<Item> resource = new Resource<Item>(item,
                    linkTo(ProductController.class).slash("/items").slash(item.getId()).withSelfRel());
            resource.add(linkTo(AccountController.class).slash(item.getSupplier().getReferenceId()).withRel("supplier"));
            return resource;
        }

        public List<Resource<Catalog>> toResource(List<Catalog> catalogs) {
            List<Resource<Catalog>> resources = new ArrayList<>();
            for (Catalog catalog : catalogs) {
                Resource<Catalog> resource = new Resource<>(catalog,
                        linkTo(ProductController.class).slash("/items").slash(catalog.getId()).withSelfRel());
                resources.add(resource);
            }
            return resources;
        }

    }

    public Resource<Item> toItemResource() {
        ProductResourceAssembler productResourceAssembler = new ProductResourceAssembler();
        return productResourceAssembler.toResource(item);
    }

    public List<Resource<Catalog>> toItemCatalogResource() {
        ProductResourceAssembler productResourceAssembler = new ProductResourceAssembler();
        return productResourceAssembler.toResource(catalogs);
    }

    public List<Resource<Item>> toListItemCatalogResource() {
        ProductResourceAssembler productResourceAssembler = new ProductResourceAssembler();
        List<Resource<Item>> itemsResource = new ArrayList<>();
        for (Item item : items) {
            ObjectMapper mapper = new ObjectMapper();
            mapper.disable(MapperFeature.DEFAULT_VIEW_INCLUSION);
            String result = null;
            try {
                result = mapper
                        .writerWithView(DataView.Summary.class)
                        .writeValueAsString(item);
            } catch (JsonProcessingException ex) {
                Logger.getLogger(Product.class.getName()).log(Level.SEVERE, null, ex);
            }
            itemsResource.add(productResourceAssembler.toResource(new Gson().fromJson(result, Item.class)));
        }
        return itemsResource;
    }
}
