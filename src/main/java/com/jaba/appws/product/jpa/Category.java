/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jaba.appws.product.jpa;

import com.jaba.appws.common.entity.BaseLookUp;
import javax.persistence.Entity;
import javax.persistence.Table;
import lombok.Data;

/**
 *
 * @author jaba
 */
@Data
@Entity
@Table(name = "ProductCategory")
public class Category extends BaseLookUp {

    private static final long serialVersionUID = 5927838646624801037L;

    
}
