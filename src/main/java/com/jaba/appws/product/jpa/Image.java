/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jaba.appws.product.jpa;

import com.jaba.appws.common.entity.AbstractBaseId;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;

/**
 *
 * @author jaba
 */
@Data
@Entity
@Table(name = "ProductImage")
@DynamicUpdate(true)
public class Image extends AbstractBaseId {

    private static final long serialVersionUID = 7598639557909283482L;

    @Column
    private String path;
    @Column
    private String title;
    @Column
    private String description;

    @Column(columnDefinition = "tinyint(1) default 0")
    private boolean archive;

}
