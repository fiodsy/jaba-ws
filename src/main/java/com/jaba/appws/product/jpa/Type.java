/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jaba.appws.product.jpa;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jaba.appws.common.entity.BaseLookUp;
import io.swagger.annotations.ApiModelProperty;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import lombok.Data;

/**
 *
 * @author jaba
 */
@Data
@Entity
@Table(name = "ProductType")
public class Type extends BaseLookUp {

    private static final long serialVersionUID = 5092834307112279044L;

    @ApiModelProperty(position = 0)
    @Enumerated(EnumType.STRING)
    @JsonIgnore
    Category category;

    public enum Category {
        PROFFESSIONAL("P"), FACILITY("F"), RETAIL("R"), CATERING("C");

        private final String code;

        Category(String code) {
            this.code = code;
        }

        public String getCode() {
            return this.code;
        }
    }

}
