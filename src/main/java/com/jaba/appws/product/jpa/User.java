/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jaba.appws.product.jpa;

import com.fasterxml.jackson.annotation.JsonView;
import com.jaba.appws.common.entity.AbstractBaseId;
import com.jaba.appws.common.util.DataView;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;

/**
 *
 * @author jaba
 */
@Data
@Entity
@Table(name = "ProductUser")
@DynamicUpdate(true)
public class User extends AbstractBaseId {

    private static final long serialVersionUID = -7738499264783392495L;
    @Column(nullable = false)
    @JsonView(DataView.Summary.class)
    private String name;
    @Column(nullable = false)
    @JsonView(DataView.Summary.class)
    private String referenceId;
    @Column
    private String contactNumber;
    @Column
    private String email;
    @Column
    @JsonView(DataView.Summary.class)
    private String accountType;
    @Column
    private String address;
    @Column
    @Enumerated(EnumType.STRING)
    private Role role;

    @Override
    @JsonView(DataView.Audit.class)
    public String getId() {
        return super.getId(); //To change body of generated methods, choose Tools | Templates.
    }

    public enum Role {
        SUPPLIER, CUSTOMER;

    }

}
