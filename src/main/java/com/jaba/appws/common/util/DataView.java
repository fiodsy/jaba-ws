/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jaba.appws.common.util;

/**
 *
 * @author jaba
 */
public class DataView {

    public static class None {
    }

    public static class Summary {
    }

    public static class Detail extends Summary {
    }

    public static class Audit extends Detail {
    }

}
