/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jaba.appws.common.util;

import com.jaba.appws.account.jpa.Account;
import com.jaba.appws.account.jpa.Address;
import com.jaba.appws.account.jpa.Contact;
import com.jaba.appws.account.jpa.Organization;
import com.jaba.appws.account.jpa.Person;
import com.jaba.appws.common.vo.Name;
import com.jaba.appws.common.vo.UserDetail;
import com.jaba.appws.product.jpa.Item;
import com.jaba.appws.product.jpa.Product;
import com.jaba.appws.product.jpa.User;
import com.oracle.jrockit.jfr.Producer;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import org.mapstruct.NullValueCheckStrategy;
import org.mapstruct.NullValueMappingStrategy;
import org.mapstruct.factory.Mappers;

/**
 *
 * @author jaba
 */
@Mapper(nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface MapUtil {

    MapUtil MAPPER = Mappers.getMapper(MapUtil.class);

    /* ACCOUNT */
    Account updateAccount(Account source, @MappingTarget Account target);

    Organization updateOrganization(Organization source, @MappingTarget Organization target);

    Person updatePerson(Person source, @MappingTarget Person target);

    Name updateName(Name source, @MappingTarget Name target);

    Address updateAddress(Address source, @MappingTarget Address target);

    Contact updateContact(Contact source, @MappingTarget Contact target);

    @Mapping(source = "id", target = "referenceId")
    User mapUserDetailToUser(UserDetail userDetail);

    /* PRODUCT */
    Product updateProduct(Product source, @MappingTarget Product product);

    Item updateItem(Item source, @MappingTarget Item target);

}
