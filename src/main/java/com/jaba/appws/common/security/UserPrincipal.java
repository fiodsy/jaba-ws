/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jaba.appws.common.security;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jaba.appws.account.jpa.Account;
import com.jaba.appws.common.vo.Access;
import com.jaba.appws.common.vo.Name;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.Embedded;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

/**
 *
 * @author jaba
 */
@Getter
public class UserPrincipal implements UserDetails {

    private static final long serialVersionUID = 1757497660149431132L;

    private String id;

    private String usernameOrEmail;

    @JsonIgnore
    private String password;

    @Embedded
    private Name name;

    private String accountType;

    private boolean verified;

    private Collection<? extends GrantedAuthority> authorities;



    public UserPrincipal() {
    }

    public UserPrincipal(String id, String usernameOrEmail, String password, Collection<? extends GrantedAuthority> authorities) {
        this.id = id;
        this.usernameOrEmail = usernameOrEmail;
        this.password = password;
        this.authorities = authorities;
    }

    public UserPrincipal(String id, String usernameOrEmail, String password, Name name, String accountType) {
        this.id = id;
        this.usernameOrEmail = usernameOrEmail;
        this.password = password;
        this.name = name;
        this.accountType = accountType;
    }

    public UserPrincipal create(Account account) {

        List<GrantedAuthority> authorities = account.getAccess().getPermissions().stream().map(permission ->
                new SimpleGrantedAuthority(permission)
        ).collect(Collectors.toList());

        return new UserPrincipal(account.getId(),
                account.getUsername(),
                account.getPassword(), authorities);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return usernameOrEmail;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return !verified;
    }

}
