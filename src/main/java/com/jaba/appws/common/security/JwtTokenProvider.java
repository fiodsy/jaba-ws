/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jaba.appws.common.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import java.util.Date;
import java.util.logging.Logger;
import static org.hibernate.annotations.common.util.impl.LoggerFactory.logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.Authentication;

/**
 *
 * @author jaba
 */
@Configuration
public class JwtTokenProvider {

    private static final Logger logger = Logger.getLogger(JwtTokenProvider.class.getName());
    
    

    @Value("${app.jwtSecret}")
    private String jwtSecret;

    @Value("${app.jwtExpirationInMs}")
    private int jwtExpirationInMs;

    public String generateToken(Authentication authentication) {
        UserPrincipal principal = (UserPrincipal) authentication.getPrincipal();
        Date issuedDate = new Date();
        Date expiryDate = new Date(issuedDate.getTime() + jwtExpirationInMs);

        Claims claims = Jwts.claims().setSubject(principal.getId())
                .setIssuedAt(issuedDate)
                .setExpiration(expiryDate);
        claims.put("usernameOrEmail", principal.getUsername());
        claims.put("name", principal.getName());
        claims.put("verified", principal.isVerified());
        claims.put("authorities", principal.getAuthorities());

        return Jwts.builder().setClaims(claims).signWith(SignatureAlgorithm.HS512, jwtSecret).compact();
    }
    
    
     public String getUserIdFromJWT(String token) {
        Claims claims = Jwts.parser()
                .setSigningKey(jwtSecret)
                .parseClaimsJws(token)
                .getBody();

        return claims.get("usernameOrEmail", String.class);
    }

    public boolean validateToken(String authToken) {
        try {
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
            return true;
        } catch (SignatureException ex) {
            logger.severe("Invalid JWT signature");
        } catch (MalformedJwtException ex) {
            logger.severe("Invalid JWT token");
        } catch (ExpiredJwtException ex) {
            logger.severe("Expired JWT token");
        } catch (UnsupportedJwtException ex) {
            logger.severe("Unsupported JWT token");
        } catch (IllegalArgumentException ex) {
            logger.severe("JWT claims string is empty.");
        }
        return false;
    }

}
