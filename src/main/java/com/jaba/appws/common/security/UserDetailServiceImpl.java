/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jaba.appws.common.security;

import com.jaba.appws.account.domain.AccountRepository;
import com.jaba.appws.account.domain.IAccountService;
import com.jaba.appws.account.jpa.Account;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 *
 * @author jaba
 */
@Service
public class UserDetailServiceImpl implements UserDetailsService {

    private static final Logger LOG = Logger.getLogger(UserDetailServiceImpl.class.getName());
   
    @Autowired    
    IAccountService accountService;
    
    @Override
    public UserDetails loadUserByUsername(String usernameOrEmail) throws UsernameNotFoundException {
        Account account = accountService.findByUsernameOrEmail(usernameOrEmail);
        LOG.info("Account " + account.toString());
        return new UserPrincipal().create(account);
    } 
    
}
