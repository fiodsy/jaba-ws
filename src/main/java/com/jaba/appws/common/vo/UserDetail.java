/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jaba.appws.common.vo;

import java.io.Serializable;
import javax.persistence.Embeddable;
import lombok.Data;

/**
 *
 * @author jaba
 */
@Data
public class UserDetail implements Serializable {

    private String name;

    private String email;

    private String address;

    private String contactNumber;

    private String accountType;

    private String id;

}
