/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jaba.appws.common.vo;

import com.fasterxml.jackson.annotation.JsonView;
import com.jaba.appws.common.util.DataView;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import javax.persistence.Embeddable;
import lombok.Data;
import lombok.Value;

/**
 *
 * @author jaba
 */
@Data
@Embeddable
public class Name implements Serializable {

    private static final long serialVersionUID = 5845359840677519845L;

    @ApiModelProperty(position = 0)
    @JsonView(DataView.Detail.class)
    private String title;
    @ApiModelProperty(position = 1)
    @JsonView(DataView.Summary.class)
    private String firstName;
    @ApiModelProperty(position = 3)
    @JsonView(DataView.Summary.class)
    private String lastName;
    @ApiModelProperty(position = 2)
    @JsonView(DataView.Detail.class)
    private String middleName;

}
