/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jaba.appws.common.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.jaba.appws.account.domain.RoleRepository;
import com.jaba.appws.account.jpa.Permission;
import com.jaba.appws.account.jpa.Role;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.Value;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author jaba
 */
@Data
@Embeddable
public class Access implements Serializable {

    private static final long serialVersionUID = -4436742654923451575L;

    public Access() {
    }

    public Access(String role) {
        this.role = role;
    }

    @Column
    private String role;
    
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private transient Set<String> permissions;

    public Access role(String role) {
        this.role = role;
        return this;
    }


}
