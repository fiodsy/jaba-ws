/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jaba.appws.common.vo;

import com.jaba.appws.account.jpa.Permission;
import io.swagger.annotations.ApiModelProperty;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Value;
import lombok.experimental.NonFinal;

/**
 *
 * @author jaba
 */
@Embeddable
public class AccessType {

    @ApiModelProperty(position = 0)
    @Enumerated(EnumType.STRING)
    Type type;

    public enum Type {
        ADMIN, USER
    }

    public AccessType setUser() {
        this.type = Type.USER;
        return this;
    }

    public AccessType setAdmin() {
        this.type = Type.ADMIN;
        return this;
    }
}
