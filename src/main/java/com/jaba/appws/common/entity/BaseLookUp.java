/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jaba.appws.common.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author jaba
 */
@MappedSuperclass
@Getter
@Setter
public class BaseLookUp extends AbstractBaseCode {

    private static final long serialVersionUID = 5165286107060187398L;

    @Column
    private String name;

    @Column
    private String description;

    @Column(columnDefinition = "tinyint(1) default 0")
    @JsonIgnore
    private boolean deleted;

}
