/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jaba.appws.common.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonView;
import com.jaba.appws.common.util.DataView;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author jaba
 */
@MappedSuperclass
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AbstractBaseId implements Serializable {

    private static final long serialVersionUID = -6638377077104761462L;

    @ApiModelProperty(hidden = true)
    @Column(nullable = false, unique = true)
    @JsonView(DataView.Summary.class)
    private String id;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false)
    @JsonIgnore
    private Long sequenceId;

    @PrePersist
    private void assignUUID() {
        setId(UUID.randomUUID().toString());
    }

}
