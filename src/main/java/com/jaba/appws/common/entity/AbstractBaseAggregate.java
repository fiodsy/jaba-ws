/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jaba.appws.common.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author jaba
 */
@MappedSuperclass
@Getter
@Setter
public class AbstractBaseAggregate extends AbstractBaseId implements Serializable {

    private static final long serialVersionUID = -6638377077104761462L;

}
