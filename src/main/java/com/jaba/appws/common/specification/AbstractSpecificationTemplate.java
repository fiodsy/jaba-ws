/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jaba.appws.common.specification;

import javax.persistence.criteria.ListJoin;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;

/**
 *
 * @author jaba
 */
@Service
public abstract class AbstractSpecificationTemplate<T, U> {

    private final String wildcard = "%";

    public abstract Specification<T> getFilter(U request);

    protected String containsLowerCase(String searchField) {
        return wildcard + searchField.toLowerCase() + wildcard;
    }

    public Specification<T> equal(String key, Object value) {
        if (!areInputsValid(key, value)) {
            return null;
        }

        return (root, query, builder) -> {
            return builder.equal(root.get(key), value);
        };

    }

    public Specification<T> like(String key, Object value) {
        if (!areInputsValid(key, value)) {
            return null;
        }

        return (root, query, builder) -> {
            return builder.like(root.get(key), "%" + value.toString() + "%");
        };

    }

    public Specification<T> notEqual(String key, Object value) {
        if (!areInputsValid(key, value)) {
            return null;
        }

        return (root, query, builder) -> {
            return builder.notEqual(root.get(key), value); //To change body of generated lambdas, choose Tools | Templates.
        };
    }

    public Specification<T> equalNull(String key) {
        if (!isKeyValid(key)) {
            return null;
        }
        return (root, query, buider) -> {
            return buider.isNull(root.get(key)); //To change body of generated lambdas, choose Tools | Templates.
        };
    }

    public Specification<T> notEqualNull(String key) {
        if (!isKeyValid(key)) {
            return null;
        }
        return (root, query, buider) -> {
            return buider.notEqual(root.get(key), null); //To change body of generated lambdas, choose Tools | Templates.
        };
    }

    public Specification<T> equalToId(String value) {
        if (!isValueValid(value)) {
            return null;
        }

        return (root, query, builder) -> {
            return builder.equal(root.get("id"), value); //To change body of generated lambdas, choose Tools | Templates.
        };
    }

    public Specification withDeleted(boolean deleted) {
        return this.equal("deleted", deleted);
    }

    protected boolean isKeyValid(String key) {
        return key != null && !key.isEmpty();
    }

    protected boolean isValueValid(String value) {
        return value != null && !value.isEmpty();
    }

    protected boolean areInputsValid(String key, Object value) {
        return key != null && value != null && !key.isEmpty() && !value.toString().isEmpty();
    }

}
